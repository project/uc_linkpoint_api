UC Linkpoint API
=========================
Enables a payment method in Ubercart that allows users to pay via the
Linkpoint API (also known as First Data).
http://www.linkpointcentral.com/

Features
=========================
- Submits the following information to Linkpoint: Order ID, User ID, Email,
  Billing Address, Shipping Address, Credit Card Info, Total Amount, Order
  products (in the comments section).

  NOTE: Order IDs must be unique in the Linkpoint system.  For this reason,
  the module appends a timestamp to the end of every order number that is
  sent to First Data. The module previously appended an incrementing
  transaction attempt number. HOWEVER, Linkpoint also
  has a Fraud Setting under Administration -> Basic Fraud Settings which
  disallows duplicate orders (defined as same amount with same credit card
  number) within X number of minutes.  You may want to change this to lower
  than the default of 10 minutes or your customers may still experience
  declined transactions.

  NOTE: The User ID is only recorded for users who are logged in when they
  check out.

Installation Instructions
=========================
1) Download the module.
2) Untar it into sites/all/modules and enable it in Drupal.
3) Navigate to the Payment settings in Ubercart (admin/store/settings/payment/edit/gateways).
   Click on "Linkpoint API settings" and fill in the settings (see below).
4) Store Number. A valid store number is required. Unfortunately, with LinkPoint
   YOU CAN‘T JUST USE ANY OLD STORE NUMBER.
5) PEM File.  Also, you can‘t just generate your own PEM file.  You‘ll need to
   use a special PEM file provided by LinkPoint.  To get it, go to Linkpoint
   Central -> Support -> Download Center.  Use the API button to save the PEM
   file to a path on your server that is protected and not web-accessible.
6) In the Payment settings you will also want to set Linkpoint as the Default
   Gateway for credit cards.
7) Ensure that you have the PHP curl module installed on your server and test
   it out.

Reference
=========================
http://www.firstdata.com/support/manuals_and_guides/global_gateway.htm

Credits
=========================
Developed by Elvis McNeely (http://www.lafayetteweb.com | ubercart@mcneelycorp.com | customizations possible).
Revision and further integration by Nicolas Soro | nicksoro@gmail.com.
Porting to Ubercart 2.x/Drupal 6.x by Sean Corrales | sean.corrales@interworksinc.com
6.x Version released by Jason Rust | jason@rustedcode.com
Originally contrib:
http://www.ubercart.org/contrib/903
